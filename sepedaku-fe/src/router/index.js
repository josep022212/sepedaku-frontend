import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home/:userId',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/profile/:userId',
    name: 'Profile',
    component: () => import('../views/Profile.vue')
  },
  {
    path:'/product/:userId',
    name:'Product',
    component: () => import('../views/Product.vue')
  },
  {
    path:'/product/:userId/:id',
    name:'ProductDetail',
    component: () => import('../views/ProductDetail.vue')
  },
  {
    path:'/product/:userId/sort/:id',
    name:'Sort',
    component: () => import('../views/Sort.vue')
  },
  {
    path: '/register-user',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/credit-card/:userId',
    name: 'creditCard',
    component: () => import('../views/CreditCard.vue')
  },
  {
    path: '/add-address/:userId',
    name: 'AddAddress',
    component: () => import('../views/AddAddress.vue')
  },
  //ADMIN
  {
    path: '/admin/product/',
    name: 'AdminProduct',
    component: () => import('../views/Admin/AdminProduct.vue')
  },
  {
    path: '/admin/product/add/',
    name: 'AdminProductAdd',
    component: () => import('../views/Admin/Add_Product.vue')
  },
  {
    path: '/admin/product/edit/:id',
    name: 'AdminProductEdit',
    component: () => import('../views/Admin/Edit_Product.vue')
  },
  {
    path: '/admin/user/',
    name: 'AdminUser',
    component: () => import('../views/Admin/AdminUser.vue')
  },
  {
    path: '/admin/user/add/',
    name: 'AdminUserAdd',
    component: () => import('../views/Admin/Add_User.vue')
  },
  {
    path: '/admin/sales/',
    name: 'Sales',
    component: () => import('../views/Admin/SalesOrder.vue')
  },
  {
    path: '/admin/purchase/',
    name: 'Purchase',
    component: () => import('../views/Admin/PurchaseOrder.vue')
  },
  {
    path: '/admin/purchase/add/',
    name: 'PurchaseAdd',
    component: () => import('../views/Admin/Add_PurchaseOrder.vue')
  },
  {
    path: '/payment-transaction/information/:userId',
    name: 'PaymentInformation',
    component: () => import('../views/PaymentTransaction/Information.vue')
  },
  {
    path: '/payment-transaction/payment/:userId',
    name: 'PaymentPayment',
    component: () => import('../views/PaymentTransaction/Payment.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
